package com.example.threadtest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "signalThread";
    Thread b;
    private List<Integer> taskQueue;
    private List<Integer> taskQueuefin;
    final int[] cnt = {0};


    class ThreadB extends Thread{
        int value;
        @Override
        public void run(){
            while(true) {
                synchronized (taskQueue) {
                    try {
                        Log.d(TAG, "wait");
                        if(taskQueue.isEmpty()) {
                            taskQueue.wait();
                        }
                        value = taskQueue.remove(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, "finish:"+value);

                }
                synchronized (taskQueuefin) {
                    taskQueuefin.add(value + 100);
                    taskQueuefin.notify();

                    Log.d(TAG, "finish2:" + value);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b = new ThreadB();
        b.start();

        taskQueue = new ArrayList<Integer>();
        taskQueuefin = new ArrayList<Integer>();

        Button btn = findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "taskqueue start.");
                synchronized(taskQueue){
                    taskQueue.add(cnt[0]);
                    cnt[0]++;
                    taskQueue.notify();

                }
                Log.d(TAG, "taskqueuefin wait.");
                synchronized (taskQueuefin){
                    try {
                        if(taskQueuefin.isEmpty()) {
                            taskQueuefin.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    int value = taskQueuefin.remove(0);
                    Log.d(TAG, "taskQueuefin.:"+value);
                }
                Log.d(TAG, "queue fin.");
            }
        });
    }
}